%global debug_package %{nil}
%define zeppelin_home /opt/zeppelin

Name: zeppelin
Version: 0.10.1
Release: 1%{?dist}
Summary: A web-based notebook that enables interactive data analytics

License: GPL-2.0-only and OFL-1.1 and JSON and BSD and LGPL-2.0-only and Apache-2.0 and EPL-1.0 and LGPL-2.1-only and CC0-1.0 and Public Domain and MIT
Source0: zeppelin-%{version}.tar.gz
Source1: spark_2.4.5_src.tar.gz
Patch0: remove_addition_repository.patch
Patch1: rlang_skip_download.patch
Patch2: run_bower_install.patch
Patch3: flink_skip_download.patch
Patch4: spark_phantomjs_download.patch
Patch5: spark_skip_download.patch
Patch6: remove_scalding.patch
Patch7: use_huawei_cloud_for_node.patch


BuildRequires: git java-1.8.0-openjdk-devel maven nodejs
Provides: Zeppelin = %{version}
Requires: java-1.8.0-openjdk systemd nodejs hostname

%description
Web-based notebook that enables data-driven, interactive data analytics and collaborative documents with SQL, Scala and more.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p0
%patch1 -p0
%patch2 -p0
%patch3 -p0
%patch4 -p0
%patch5 -p0
%patch6 -p0
%patch7 -p0

tar xf %{S:1}

%build
mvn clean package -Pbuild-distr -Pspark-2.4 -Pspark-scala-2.11 -DskipTests

%install
install -d -m 0755 $RPM_BUILD_ROOT%{zeppelin_home}
install -d -m 0755 $RPM_BUILD_ROOT%{zeppelin_home}/{bin,interpreter,k8s,lib,plugins}
install -d -m 0755 $RPM_BUILD_ROOT%{zeppelin_home}/conf
install -d -m 0755 $RPM_BUILD_ROOT%{zeppelin_home}/notebook

cd zeppelin-distribution/target/zeppelin-%{version}-SNAPSHOT/zeppelin-%{version}-SNAPSHOT
cp -a {bin,interpreter,k8s,lib,plugins,zeppelin-web-*.war} $RPM_BUILD_ROOT%{zeppelin_home}/
cp -a conf/* $RPM_BUILD_ROOT%{zeppelin_home}/conf/
cp -a notebook/* $RPM_BUILD_ROOT%{zeppelin_home}/notebook/

rm -f $RPM_BUILD_ROOT%{zeppelin_home}/bin/*.cmd
chmod 755 $RPM_BUILD_ROOT%{zeppelin_home}/bin/*
exit 0

%pre
getent group zeppelin >/dev/null || groupadd -r zeppelin
getent passwd zeppelin >/dev/null || useradd -c "Zeppelin" -s /sbin/nologin -g zeppelin -r zeppelin 2> /dev/null || :

%files
%defattr(-,zeppelin,zeppelin,755)
%doc NOTICE LICENSE
%{zeppelin_home}

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Sun Dec 26 2021 Hao Zhang <unioah@isrc.iscas.ac.cn>
- remove install nodejs due to CI restriction
* Sun Nov 21 2021 Hao Zhang <unioah@isrc.iscas.ac.cn>
- remove scalding for jars in private repo due to CI restriction
* Sat Sep 25 2021 Hao Zhang <unioah@isrc.iscas.ac.cn>
- update to 0.10.1
* Sun Nov 22 2020 Hao Zhang <unioah@isrc.iscas.ac.cn>
- add cached spark tar files for flink, R due to OBS restriciton
* Sat Oct 10 2020 Hao Zhang <unioah@isrc.iscas.ac.cn>
- init repo
