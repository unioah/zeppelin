Zeppelin

---
Web-based notebook that enables data-driven, interactive data analytics and collaborative documents with SQL, Scala and more.
https://zeppelin.apache.org/docs/0.7.0/install/build.html
